def getResults(str1: str) -> [(str,str)]:
	a = 0
	a_win = 0
	b = 0
	b_win = 0 
	for i in str1:
		if i == 'A':
			a += 1
		else:
			b += 1
		score = getScore(a, b)
	
def getScore(a: int, b: int):
	names = { 0:'Love', 1: '15',2: '30',3: '40'}	
	if a not in names or b not in names:
		return diffResult(a,b)
	else:
		return names[a]+names[b]

def diffResult(a: int,b: int):
	if a-b == 0:
		return 'deuce'
	elif abs(a-b) == 1:
		return 'advantage'
	else:
		return 'game'
import sys
print(getResults(sys.argv[1]))
